# Monday-Tuesday-Wednesday

These shifts have been organized according to students preferences, maximizing accesses as much as possible, while respecting distances between desks, i.e. keeping [ABCD groups structure](occasional.md) as much as possible.

## Room 127 - Marconi

| Day           | Student       |
| ------------- |:-------------:|
| Monday	|   Piombo, Scardino    |
| Tuesday       |   Piombo, Scardino    |
| Wednesday     |   Piombo, Scardino    |

## Room 339b - Marconi 
 
| Day           | Student      |    
| ------------- |:------------:|
| Wednesday     | Rota         |

## Room 117-118 - Fermi 

| Day           | Student       |
| ------------- |:-------------:|
| Monday        |   Diaz                    |
| Tuesday       |   Pisegna                 |
| Wednesday     |   Sabatucci,Franco        |

## Room 413 - Fermi

| Day           | Student       |
| ------------- |:-------------:|
| Monday        |   Valeri      |
| Tuesday       |   Valeri      |
| Wednesday     |   Valeri      |