# Monday-Tuesday-Wednesday

These shifts have been organized according to students preferences, maximizing accesses as much as possible, while respecting distances between desks, i.e. keeping [ABCD groups structure](occasional.md).

## Room 127 - Marconi

| Day           | Student       |
| ------------- |:-------------:|
| Monday	| Venditti      |
| Tuesday       | Venditti      |
| Wednesday     | Venditti      |

## Room 314 - Fermi

| Day           | Student       |
| ------------- |:-------------:|
| Monday        | Niedda        |
| Tuesday       | Borra         |
| Wednesday     | Borra, Niedda |

## Room 117-118 - Fermi

| Day           | Student               |
| ------------- |:---------------------:|
| Monday        | Sabatucci, Franco     |
| Tuesday       | Del Monte, Piacentini |
| Wednesday     | Franco, Carmona Sosa  |