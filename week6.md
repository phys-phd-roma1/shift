# Monday-Tuesday-Wednesday

These shifts have been organized according to students preferences, maximizing accesses as much as possible, while respecting distances between desks, i.e. keeping [ABCD groups structure](occasional.md).

## Room 127 - Marconi

| Day           | Student       |
| ------------- |:-------------:|
| Monday	|   Piombo, Scardino    |
| Tuesday       |   Venditti    |
| Wednesday,  9-13   |   Piombo, Scardino    |
| Wednesday, 14-18   |   Venditti    |

## Room 117-118 - Fermi

| Day           | Student       |
| ------------- |:-------------:|
| Monday        |   Diaz        |
| Wednesday     |   Diaz        |

## Room 413 - Fermi

| Day           | Student       |
| ------------- |:-------------:|
| Monday        |   Esposito    |
| Tuesday       |   Esposito    |
| Wednesday     |   Esposito    |