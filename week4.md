
# Monday-Tuesday-Wednesday

These shifts have been organized according to students preferences, maximizing accesses as much as possible, while respecting distances between desks, i.e. keeping [ABCD groups structure](occasional.md).

## Room 127 - Marconi

| Day           | Student       |
| ------------- |:-------------:|
| Monday	|   Venditti    |
| Tuesday       |   Venditti    |
| Wednesday     |   Venditti    |

## Room 117-118 - Fermi

| Day           | Student       |
| ------------- |:-------------:|
| Monday, 9-13  |   Sabatucci   |
| Monday, 14-18 |   Piacentini  |
| Tuesday       |   Sabatucci   |
| Wednesday     |   Piacentini  |