from itertools import combinations,product
from pprint import pprint
import numpy as np

people={"sabatucci" : {"group":"B", "day":[1,2,3], "ndays": 2},
        "franco"    : {"group":"B", "day":[1,2,3], "ndays": 2},
        "pisegna"   : {"group":"D", "day":[2,3],   "ndays": 2},
        "diaz"      : {"group":"C", "day":[1,3],   "ndays": 2}
        }

comb1=list(combinations(people,2))



shift=list(product(comb1,repeat=3))

scores=[]

for s in shift:
    score=0
    for d in xrange(len(s)):
        day=d+1
        
        if people[s[d][0]]["group"]==people[s[d][1]]["group"]:
            score+=1
        
        if day in people[s[d][0]]["day"]:
            score+=1
        if day in people[s[d][1]]["day"]:
            score+=1


    a=np.array(s)
    a=a.flatten()
    a=list(a)

    
    for p in people:
        score += a.count(p)-people[p]["ndays"] if a.count(p)-people[p]["ndays"]>0 else 5*(a.count(p)-people[p]["ndays"])

    scores.append([s, score])

scores.sort(key=lambda x: x[1])

pprint(scores)
