# Ph.D. students access organization for upcoming weeks
This page contains weekly Ph.D.'s shifts for accessing the Physics Department during Phase-2.

Shifts have been organized in the following way:
* Monday to Wednesday: dedicated to those who made a request for one or more than one access per week on the form
* Thursday and Friday: occasional access for everyone

This page will be updated every week, according to the outcomes of this [form](https://docs.google.com/forms/d/e/1FAIpQLSf_-xpFVLzkvhaT4rEcUR_szxSJqm5mFMlqHyM2DrULk0zgxw/viewform?usp=sf_link).

We want to stress that this kind of room reservation is intended to minimize contacts in the common PhD spaces and **keep track** of them. We invite everyone to fill properly the above mentioned form and **respect the shifts**. 

## Important Update

Starting from October 4th, all the bookings are managed by the web-app at [phdbooking.pythonanywhere.com](phdbooking.pythonanywhere.com). All the information has been sent on the official mailing list. Please check your e-mails or contact PhD representatives in case of troubles.

Last update: <today>
