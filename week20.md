# Monday-Tuesday-Wednesday

These shifts have been organized according to students preferences, maximizing accesses as much as possible, while respecting distances between desks, i.e. keeping [ABCD groups structure](occasional.md) as much as possible. 

#### UPDATE
Due to **new Sapienza prescriptions**, room capacities have been increased and ABCD scheme is less stringent: there is still the constraint of forbidden nearest neighbors.

We want to stress that this kind of room reservation is intended to minimize contacts in the common PhD spaces and **keep track** of them. We invite everyone to **respect the shifts**. 


## Room 127 - Marconi
This room asked to realease ABCD scheme for Thursday and Friday.

| Day           | Student       | 
| ------------- |:-------------:| 
| Monday        | Cherubini, Venditti, Castro, Raposo, Scardino   |
| Tuesday       | Cherubini, Venditti, Piombo, Udina    |
| Wednesday     | Cherubini, Venditti, Castro, Raposo, Scardino   |
| Thursday      | Cherubini, Venditti, Piombo, Udina    |
| Friday        | Cherubini, Venditti, Castro, Raposo, Scardino   |


## Room 339b - Marconi
This room asked to realease ABCD scheme for Thursday.

| Day           | Student       | 
| ------------- |:-------------:| 
| Monday        | Piovano       |
| Tuesday       | Maggio, Neuwirth |
| Wednesday     | Piovano       |
| Thursday      | Neuwirth      |


## Room 117-118 - Fermi
| Day           | Student       | 
| ------------- |:-------------:| 
| Monday        | Camerin, Diaz, Franco |
| Tuesday       | Camerin, Diaz, Franco | 
| Wednesday     | Camerin, Diaz, Franco |


## Room 413 - Fermi 
This room is currently beta-testing the new booking app. If usefull and stable, the app will be used for all rooms.

