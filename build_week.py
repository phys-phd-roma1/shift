import os

introMessage="""
# Monday-Tuesday-Wednesday

These shifts have been organized according to students preferences, maximizing accesses as much as possible, while respecting distances between desks, i.e. keeping [ABCD groups structure](occasional.md)."""

roomTable="""
## Room <roomName>

| Day           | Student       |
| ------------- |:-------------:|
| Monday        |     <mon>     |
| Tuesday       |     <tue>     |
| Wednesday     |     <wed>     |"""



