# Monday-Tuesday-Wednesday

These shifts have been organized according to students preferences, maximizing accesses as much as possible, while respecting distances between desks, i.e. keeping [ABCD groups structure](occasional.md).

## Room 127 - Marconi

| Day           | Student       |
| ------------- |:-------------:|
| Monday	|   <mon-127>   |
| Tuesday       |   <tue-127>   |
| Wednesday     |   <wed-127>   |

## Room 314 - Fermi

| Day           | Student       |
| ------------- |:-------------:|
| Monday        |   <mon-314>   |
| Tuesday       |   <tue-314>   |
| Wednesday     |   <wed-314>   |

## Room 117-118 - Fermi

| Day           | Student       |
| ------------- |:-------------:|
| Monday        |   <mon-117>   |
| Tuesday       |   <tue-117>   |
| Wednesday     |   <wed-117>   |