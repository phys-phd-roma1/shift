# Monday-Tuesday-Wednesday

These shifts have been organized according to students preferences, maximizing accesses as much as possible, while respecting distances between desks, i.e. keeping [ABCD groups structure](occasional.md) as much as possible.

## Room 127 - Marconi

| Day           | Student       |
| ------------- |:-------------:|
| Monday	    |   Di Carlo, Venditti    |
| Tuesday       |   Di Carlo, Venditti    |
| Wednesday     |   Baldan      |

## Room 117-118 - Fermi 

| Day           | Student       |
| ------------- |:-------------:|
| Monday        |   Diaz        |
| Tuesday       |   Sabatucci   |
| Wednesday, 9-13      |  Diaz         |
| Wednesday, 14-18     |  Sabatucci    |