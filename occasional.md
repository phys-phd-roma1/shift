# Occasional access

All students have the possibility to access the Department on Thursday or Friday (half day), according to schedule and groups below.

## Schedule

![Table ABCD](images/schedule.png)

## Groups per room

### Room 127 - Marconi
![Room 127](images/r127.png)

### Room 339b - Marconi
![Room 339b](images/r339b.png)

### Room 117-118 - Fermi
![Room 117118](images/r117118.png)

### Room 314 - Fermi
![Room 314](images/r314.png)

### Room 413 - Fermi
![Room 413](images/r413.png)

