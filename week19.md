# Monday-Tuesday-Wednesday

These shifts have been organized according to students preferences, maximizing accesses as much as possible, while respecting distances between desks, i.e. keeping [ABCD groups structure](occasional.md) as much as possible.

We want to stress that this kind of room reservation is intended to minimize contacts in the common PhD spaces and **keep track** of them. We invite everyone to **respect the shifts**. 


## Room 127 - Marconi
| Day           | Student       | 
| ------------- |:-------------:| 
| Tuesday       | Raposo        |


## Room 339b - Marconi
| Day           | Student       | 
| ------------- |:-------------:| 
| Monday        | Piovano       |
| Tuesday       | Maggio        |
| Wednesday     | Piovano       |


## Room 117-118 - Fermi
| Day           | Student       | 
| ------------- |:-------------:| 
| Monday        | Diaz, Di Carlo |
| Tuesday       | Diaz, Di Carlo | 
| Wednesday     | Diaz, Di Carlo |
